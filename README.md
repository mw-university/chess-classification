---
title: Chess Classifier
emoji: ♟️
colorFrom: white
colorTo: black
sdk: gradio
sdk_version: 3.16.1
app_file: app.py
pinned: false
---

Check out the configuration reference at https://huggingface.co/docs/hub/spaces-config-reference
